<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Assignment Aqil Nu'man</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 text-center mb-4">
            <h3>Calculate Bill</h3>
          </div>
          <div class="col-md-8">
            <form class="form-horizontal" role="form" method="POST" action="index.php">
              <div class="form-group row">
                <label for="voltage" class="col-sm-4 col-form-label text-left">Voltage (V)</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="voltage" name="voltage" value="<?php echo isset($_POST['voltage']) ? $_POST['voltage'] : ''; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="current" class="col-sm-4 col-form-label text-left">Current (A)</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="current" name="current" value="<?php echo isset($_POST['current']) ? $_POST['current'] : ''; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="currentRate" class="col-sm-4 col-form-label text-left">Current Rate (sen/kWh)</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="currentRate" name="currentRate" value="<?php echo isset($_POST['currentRate']) ? $_POST['currentRate'] : ''; ?>">
                </div>
              </div>
              <div class="form-group row justify-content-center">
                <div class="col-sm-12 text-center">
                  <button class="btn btn-primary" id="calButton">Calculate</button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>   
    <script>
      const button = document.getElementById('calButton');

      button.addEventListener('click', () => {
        <?php
       
          $voltage = $_POST['voltage'];
          $current = $_POST['current'];
          $currentRate = $_POST['currentRate'];
          
          
          // calculate power Wh = V * A
          $power = $voltage * $current;
          
          // calculate energy (kWh = Wh * h / 1000)
          $energy = $power * 1 / 1000 ;

          // rate calculation
          $rate = $currentRate / 100;

          // calculate total (sen = kWh * sen/kWh)
          $total = $energy *  ($currentRate / 100);
      ?>



      });
    </script>    
       
        <div class="col-md-12 text-center mb-4">
          <!-- Display the calculated values -->
          <p>Power: <?php echo $power/1000;?>kw</p>
          <p>Rate: RM <?php echo $rate ?></p>
        </div>
<div class="container">
  <div class="row justify-content-center">      
    <table class="table table-striped">
      <tr>
        <th>#</th>
        <th>Hour</th>
        <th>Energy (kWh)</th>
        <th>TOTAL (RM)</th>
      </tr>
      <?php
      for ($i = 1; $i <= 24; $i++) {
        $valEnergy = $energy * $i;
        $tTotal = $total * $i;
        echo "<tr><td>$i</td><td>$i</td><td>$valEnergy</td><td>".number_format($tTotal, 2)."</td></tr>";               
      }
      ?>
    </table>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>
</html>