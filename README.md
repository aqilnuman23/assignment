# Assignment

Wan Muhammad Aqil Nu'man Bin Wan Anuar
aqilnuman23@gmail.com

Program to calculate power, energy, and the total charge based on the current rate.
It is measured in kWh (kilowatt-hours). The rate of an electricity bill varies. Suppose we have the following conditions for calculating the rates of electricity:
Power (Wh) = Voltage (V) * Current  (A)
Energy (kWh) = Power * Hour * 1000 ;
Total = Energy(kWh) * (current rate/100);
